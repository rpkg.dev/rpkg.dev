# rpkg.dev

...

## Shared assets

### GoatCounter JS

To update the script, run (from the root of the repo):

``` sh
curl --silent https://gc.zgo.at/count.js | minify --type=js | sd '\s*$' '\n' > public/goatcounter.min.js
```

### Counter.dev JS

To update the script, run (from the root of the repo):

``` sh
curl --silent https://cdn.counter.dev/script.js | minify --type=js | sd '\s*$' '\n' > public/counter-dev.min.js
```

## License

Code and configuration in this repository are licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[LICENSE.md](LICENSE.md).
